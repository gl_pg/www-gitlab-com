---
layout: handbook-page-toc
title: "CSM/CSE Webinar Calendar"
---
# On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

View the [CSM Handbook homepage](/handbook/customer-success/csm/) for additional CSM/CSE-related handbook pages.

Watch previously recorded webinars on our [YouTube Playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0Kpczt4pRtyF147Uvn2bGGvq).

---

# Upcoming Webinars

We’d like to invite you to our free upcoming webinars for the Americas and EMEA timezones in the month of February.

If you know someone who might be interested in attending, feel free to share the registration links with them. Everyone is welcome, and we hope to see you there!

## February 2023

### Americas Webinars

#### Intro to GitLab
##### February 2nd, 2023 at 12:00PM-1:00PM Eastern Time/5:00-6:00 PM UTC

Are you new to GitLab? Join this webinar, where we will review what GitLab is, how it benefits you, and the recommended workflow to allow you to get the most out of the platform

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_jQ1SyCepTWymNRmmVuzT4Q)

#### Intro to CI/CD
##### February 8th, 2023 at 12:00PM-1:00PM Eastern Time/5:00-6:00 PM UTC

Come learn about what CI/CD is and how it can benefit your team. We will cover an overview of CI/CD and what it looks like in GitLab. We will also cover how to get started with your first CI/CD pipeline in GitLab and the basics of GitLab Runners.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_g76U2DbZRE-XeYOWCbdIQQ)

#### Advanced CI/CD
##### February 21st, 2023 at 12:00PM-1:00PM Eastern Time/5:00-6:00 PM UTC

Expand your CI/CD knowledge while we cover advanced topics that will accelerate your efficiency using GitLab, such as pipelines, variables, rules, artifacts, and more. This session is intended for those who have used CI/CD in the past.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_9cCjKrT3R1-pGKOCYM--sQ)

#### DevSecOps/Compliance
##### February 23rd, 2023 at 12:00PM-1:00PM Eastern Time/5:00-6:00 PM UTC

GitLab enables developers and security to work together in a single tool, allowing for proactive security or “shifting left”. This session will cover what GitLab offers, how scan results integrate seamlessly with merge requests, and how to use the Security Dashboard to manage vulnerabilities.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_TPHOE_SFRQGZzGnTJ-HJ2Q)

#### Git Basics
##### February 28th, 2023 at 12:00PM-1:00PM Eastern Time/5:00-6:00 PM UTC

Are you new to Git? Join this webinar targeted at beginners working with source code, where we will review the basics of using Git for version control and how it works with GitLab to help you get started quickly.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_aqE37VmFQmaoKHvbMn2imQ)

### EMEA Webinars

#### DevSecOps/Compliance
##### February 7th, 2023 at 10:00AM-11:00AM UTC

GitLab enables developers and security to work together in a single tool, allowing for proactive security or “shifting left”. This session will cover what GitLab offers, how scan results integrate seamlessly with merge requests, and how to use the Security Dashboard to manage vulnerabilities.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_gTxN_OLCQ3C0ErSha28mRw)

Check back later for more webinars! 


